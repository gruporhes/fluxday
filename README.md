# fluxday

fluxday is a task & productivity management application ideal for fast growing startups and small companies. fluxday was developed by Foradian starting in 2014 and was a critical part of the company’s [hyper growth and success](http://www.fedena.com/history). fluxday was opensourced by [Foradian](http://foradian.com) in May 2016 to help more startups use the power of a no-nonsense productivity tracking tool.

fluxday is engineered based on the concepts of [OKR](https://en.wikipedia.org/wiki/OKR) - Objectives and Key Results, invented and made popular by John Doerr. OKRs and OKR tools are used today by many companies, including Google, LinkedIn and Twitter

## You can use fluxday for

- Managing and Tracking OKRs
- Creating, assigning and tracking tasks
- Maintaining log of time spent by employees
- Generating different types of reports, and in different formats
- Analyzing progress and productivity of your company, its departments, teams and employees
- OAuth server with filtered access to users

Visit the [official website](http://fluxday.io) for more info

> “through discipline comes freedom” - aristotle

## License

Fluxday is released under [Apache License 2.0](https://github.com/foradian/fluxday/blob/master/LICENSE)

## How to use

This is a fork of fluxday focused exclusively on Docker. For other installation methods, please refer to the [official fluxday repository](https://github.com/foradian/fluxday).

### Prerequisites

1. Install [Docker CE](https://docs.docker.com/install/)
2. If you are on Linux, install [Docker Compose](https://docs.docker.com/compose/install/)
3. Install [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

### Installation

Clone this repository (the `/fluxday` directory is just a suggestion; use whatever you want).

```sh
git clone https://gitlab.com/gruporhes/fluxday /fluxday
cd /fluxday
```

Copy the example configuration as `example.env` as `.env` and use your favorite editor to set up your database credentials.

```sh
cp example.env .env
nano .env
```

Start the stack with:

```sh
docker-compose up -d --build
```

Wait until Docker pulls/builds the images and starts all the parts of the stack.

Fluxday can be accessed from the browser by navigating to `http://HOSTNAME`, where `HOSTNAME` is `localhost` if you are running on your machine, or the remote host name/IP address if running on a server.

#### Initial login credentials

Email: `admin@fluxday.io`  
Password: `password`

## Screenshots

###### Dashboard - View tasks and work logs for a selected day. You can switch between month and week views.

![Dashboard](http://fluxday.io/img/screenshots/dashboard_day.jpg "Dashboard")

###### Departments and Teams - Create and manage departments. Create teams within departments and add members and leads to the teams.

![Departments and Teams](http://fluxday.io/img/screenshots/department.jpg "Departments and Teams")

###### OKR - Create and manage OKRs for a user. Set custom duration for each OKR that align to your team requirements.

![OKR](http://fluxday.io/img/screenshots/okr_view.jpg "OKR")

###### Add tasks - Create tasks for users. Enter duration, map to key result and set priority for the task.

![Add tasks](http://fluxday.io/img/screenshots/add_task.jpg "Add tasks")

###### Task view - View details of tasks like assigned users, duration and priority. You can also add subtasks from here.

![Task view](http://fluxday.io/img/screenshots/task_view.jpg "Task view")

###### Reports - Generate visual and textual reports to view performance of users. Chose between OKR, Worklogs, Tasks and Assignment based reports for an employee or employee groups.

![Reports](http://fluxday.io/img/screenshots/okr_report_hi_res.jpg "Reports")
