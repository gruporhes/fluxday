#!/bin/sh

echo 'Waiting for MySQL'
while ! mysql --protocol TCP --host="${DB_HOST}" -u"${DB_USER}" -p"${DB_PASS}" -e "show databases;" > /dev/null 2>&1; do
    sleep 1
done

cd ${APP_HOME}
rake db:create
rake db:migrate
rake db:seed
exec rails server